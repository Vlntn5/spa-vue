import { createStore } from "vuex";

export default createStore({
  state: {
    conversionTable: null,
  },
  mutations: {
    setConversionTable(state, table) {
      state.conversionTable = table;
    },
  },
  getters: {
    getConversionTable(state) {
      return state.conversionTable || [];
    },
  },
  actions: {
    async getConversions(context) {
      if (context.getters.getConversionTable.length === 0) {
        try {
          let response = await fetch(
            "https://run.mocky.io/v3/0760246b-e5e8-4bee-a670-bc933b08150a"
          );
          if (response.ok) {
            let conversions = await response.json();
            context.commit("setConversionTable", conversions);
          } else {
            throw new Error("HTTP-Error: " + response.status);
          }
        } catch (error) {
          console.error(error);
        }
      }
    },
  },
});
