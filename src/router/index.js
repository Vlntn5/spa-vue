import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import NotFound from "../components/NotFound";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/currency-converter",
    name: "CurrencyConverter",
    component: () => import("../views/CurrencyConverter.vue"),
  },
  {
    path: "/currency-table",
    name: "CurrencyTable",
    component: () => import("../views/CurrencyTable.vue"),
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
